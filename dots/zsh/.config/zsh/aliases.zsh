# Misc
alias magit='e --eval "(progn (magit-status) (delete-other-windows))"'
alias -g GP=' | grep -i '
alias -g GPI=' | grep '
alias -g L=' | less '

# Filesystem
alias ls='ls -AhF'
alias ll='ls -l'
alias up="source up" # Make cd in script persist outside subshell
mkdir() {
  command mkdir $@ && cd "$_"
}

# Processes
alias psa='ps aux | grep -i '
alias psA='ps aux' # Enforce alias

# Power management
alias hibernate='systemctl hibernate'
alias suspend='systemctl suspend'
alias shutdown='shutdown -h now'

# DNF
dnf() {
  local conf="$XDG_CONFIG_HOME/dnf/dnf.conf"
  case "$1" in
    install | update) sudo dnf -c "$conf" "$@" ;;
    *)                command dnf -c "$conf" "$@"
  esac
  return $?
}
alias dni='dnf install'
alias dnup='dnf update'
alias dnin='dnf info'
alias dnse='dnf search'
alias dnrq='dnf repoquery'

# Yay
alias ys='yay -S'
alias yqs='yay -Qs'
alias yss='yay -Ss'
alias ysi='yay -Si'
alias yf='yay -F'
alias yfy='yay -Fy'
