bindkey -v
bindkey -M viins 'kj' vi-cmd-mode
bindkey -M viins '^ ' autosuggest-accept
bindkey -M viins ' ' magic-space
bindkey -M viins '^i' expand-or-complete-prefix
bindkey -M viins '^[f' vi-forward-word
bindkey -M viins '^[b' vi-backward-word
bindkey -M viins '^[^?' backward-kill-word

# History substring search
bindkey -M viins '^p' history-substring-search-up
bindkey -M viins '^n' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
