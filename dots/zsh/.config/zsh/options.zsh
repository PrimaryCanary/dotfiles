# History
HISTFILE="$XDG_CACHE_HOME/zhistory"
HISTSIZE=999999999
SAVEHIST="$HISTSIZE"
setopt APPEND_HISTORY            # Appends history to history file on exit
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits
setopt SHARE_HISTORY             # Share history between all sessions
setopt HIST_EXPIRE_DUPS_FIRST    # Expire a duplicate event first when trimming history
setopt HIST_IGNORE_DUPS          # Do not record an event that was just recorded again
setopt HIST_IGNORE_ALL_DUPS      # Delete an old recorded event if a new event is a duplicate
setopt HIST_VERIFY               # Do not execute immediately upon history expansion
setopt BANG_HIST                 # Don't treat '!' specially during expansion

# Misc
setopt EXTENDED_GLOB             # Use extended globbing syntax
