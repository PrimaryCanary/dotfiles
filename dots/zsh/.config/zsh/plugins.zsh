ZGENOM_DIR="$XDG_CACHE_HOME/zgenom"
source "$ZGENOM_DIR/zgenom.zsh"

YSU_HARDCORE=1 # Enforce alias usage
ZVM_VI_ESCAPE_BINDKEY='kj'
ZVM_INIT_MODE=sourcing # Source immediately to prevent widespread settings overwriting

zgenom autoupdate 7
if ! zgenom saved; then
    rm -f $ZDOTDIR/*.zwc(N) \
        $ZSH_CACHE_DIR/zsh/*(N) \
        "$ZGEN_INIT.zwc"
    zgenom load jeffreytse/zsh-vi-mode
    zgenom load zdharma-continuum/fast-syntax-highlighting
    #zgenom load zdharma/history-search-multi-word
    zgenom load zsh-users/zsh-completions src
    zgenom load zsh-users/zsh-autosuggestions
    zgenom load zsh-users/zsh-history-substring-search
    zgenom load MichaelAquilina/zsh-you-should-use

    zgenom load romkatv/powerlevel10k powerlevel10k
    zgenom load hlissner/zsh-autopair autopair.zsh

    zgenom save
    zgenom compile "$ZDOTDIR"
fi
