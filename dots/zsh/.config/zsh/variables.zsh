# XDG home vars
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Tool paths
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export ZSH_CACHE_DIR="$XDG_CACHE_HOME/zsh"
export GOPATH="$XDG_DATA_HOME/go"

# General shell
export PATH="$XDG_CONFIG_HOME/emacs/bin:$HOME/.local/bin:$GOPATH/bin:$PATH"
export EDITOR='e --wait'
