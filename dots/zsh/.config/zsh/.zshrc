if [ "$TERM" != "dumb" ]; then
  source "$ZDOTDIR/variables.zsh"

  # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
  # Initialization code that may require console input (password prompts, [y/n]
  # confirmations, etc.) must go above this block; everything else may go below.
  if [[ -r "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh"
  fi

  [ -d "$ZSH_CACHE_DIR" ] || mkdir -p "$ZSH_CACHE_DIR"
  autoload -Uz compinit && compinit -u -d "$ZSH_CACHE_DIR/zcompdump"

  source "$ZDOTDIR/plugins.zsh"
  source "$ZDOTDIR/keybinds.zsh"
  source "$ZDOTDIR/aliases.zsh"
  source "$ZDOTDIR/options.zsh"

  # Make GNOME Terminal tabs inherit PWD
  [ -f /etc/profile.d/vte.sh ] && source /etc/profile.d/vte.sh
  # To customize prompt, run `p10k configure` or edit $ZDOTDIR/.p10k.zsh.
  [ -f "$ZDOTDIR/.p10k.zsh" ] && source "$ZDOTDIR/.p10k.zsh"

  autopair-init
fi
