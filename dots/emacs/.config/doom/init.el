;;; init.el -*- lexical-binding: t; -*-

;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

;; NOTE Press 'SPC h d h' (or 'C-h d h' for non-vim users) to access Doom's
;;      documentation. There you'll find a link to Doom's Module Index where all
;;      of our modules are listed, including what flags they support.

;; NOTE Move your cursor over a module's name (or its flags) and press 'K' (or
;;      'C-c c k' for non-vim users) to view its documentation. This works on
;;      flags as well (those symbols that start with a plus).
;;
;;      Alternatively, press 'gd' (or 'C-c c d') on a module to browse its
;;      directory (for easy access to its source code).

(doom! :completion
       company
       (vertico +icons)

       :ui
       doom
       doom-dashboard
       hl-todo
       modeline
       ophints
       (popup +all +defaults)
       (vc-gutter +pretty)
       vi-tilde-fringe
       (window-select +numbers)
       workspaces

       :editor
       (evil +everywhere)
       file-templates
       fold
       rotate-text
       snippets

       :emacs
       (dired +icons)
       electric
       undo
       vc

       :term
       ;vterm

       :checkers
       syntax
       ;(spell +flyspell)
       ;grammar

       :tools
       ansible
       docker
       (eval +overlay)
       lookup
       lsp
       (magit +forge)
       make
       pdf

       :os
       (:if IS-MAC macos)
       ;tty

       :lang
       (cc +lsp)
       (python +lsp +pyright)
       (go +lsp)
       sh
       emacs-lisp
       data
       yaml
       (org +pretty +pandoc +roam2)
       (latex +latexmk +fold)
       markdown

       :app
       ;calendar
       ;emms
       ;everywhere
       ;irc
       (rss +org)

       :config
       (default +bindings +smartparens))
