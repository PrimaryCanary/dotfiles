#+TITLE: Doom Emacs Config
#+SUBTITLE: The Ramblings of a Madman
#+AUTHOR: Sifus
#+PROPERTY: header-args:emacs-lisp :tangle yes :cache yes :results silent :comments link :lexical yes
#+PROPERTY: header-args :tangle no :results silent
#+STARTUP: latexpreview content
#+OPTIONS: toc:nil num:nil tags:nil
* Table of Contents :TOC_2_org:
- [[Intro][Intro]]
- [[Modules][Modules]]
  - [[Input][Input]]
  - [[Completion][Completion]]
  - [[UI][UI]]
  - [[Editor][Editor]]
  - [[Emacs][Emacs]]
  - [[Term][Term]]
  - [[Checkers][Checkers]]
  - [[Tools][Tools]]
  - [[OS][OS]]
  - [[Lang][Lang]]
  - [[Email][Email]]
  - [[App][App]]
  - [[Config][Config]]
- [[General Configuration][General Configuration]]
  - [[Daemon][Daemon]]
- [[Visuals][Visuals]]
  - [[Modeline][Modeline]]
- [[Spell][Spell]]
  - [[Flyspell-Lazy][Flyspell-Lazy]]
- [[\LaTeX][\LaTeX]]
  - [[CDLaTeX][CDLaTeX]]
- [[Snippets][Snippets]]
  - [[Yasnippet][Yasnippet]]
  - [[Auto-Activating-Snippets][Auto-Activating-Snippets]]
  - [[\LaTeX​-Auto-Activating-Snippets][\LaTeX​-Auto-Activating-Snippets]]
- [[Smartparens][Smartparens]]
  - [[\LaTeX][\LaTeX]]
  - [[Org][Org]]
- [[PDF][PDF]]
- [[Org][Org]]
  - [[General Configuration][General Configuration]]
  - [[Story Archiving][Story Archiving]]
  - [[Org-Fragtog][Org-Fragtog]]
  - [[Toc-Org][Toc-Org]]
  - [[Agenda][Agenda]]
- [[Projectile][Projectile]]
- [[Evil][Evil]]
- [[Rainbow Delimiters][Rainbow Delimiters]]
- [[RSS][RSS]]
- [[Auth-source][Auth-source]]
- [[Popup][Popup]]
- [[Media Journal][Media Journal]]
  - [[Books][Books]]
  - [[TV and Movies][TV and Movies]]
- [[Chezmoi][Chezmoi]]
- [[Magit][Magit]]

* Intro
:PROPERTIES:
:header-args:emacs-lisp: :tangle no
:END:
Abandon hope, all ye who enter here, for you are about to enter the realm of madness.
* Modules
:PROPERTIES:
:header-args:emacs-lisp: :tangle no
:END:
Doom is built from the ground up with a modular architecture. These hand-made, artisanally-crafted modules are delivered directly to your ​~doom-private-dir~​, reducing the process of developing a polished Emacs configuration to something accomplishable by mere mortals. These modules are grouped by category and can be passed flags to enable additional functionality (denoted ​~:category (module +flag)~​). Let's enable some.

#+NAME: init.el
#+BEGIN_SRC emacs-lisp :tangle init.el :noweb no-export :comments none
;;;; init.el -*- lexical-binding: t; -*-

;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

;; NOTE Press 'SPC h d h' (or 'C-h d h' for non-vim users) to access Doom's
;;      documentation. There you'll find a "Module Index" link where you'll find
;;      a comprehensive list of Doom's modules and what flags they support.

;; NOTE Move your cursor over a module's name (or its flags) and press 'K' (or
;;      'C-c c k' for non-vim users) to view its documentation. This works on
;;      flags as well (those symbols that start with a plus).
;;
;;      Alternatively, press 'gd' (or 'C-c c d') on a module to browse its
;;      directory (for easy access to its source code).

(doom!
 :input
 <<doom-input>>

 :completion
 <<doom-completion>>

 :ui
 <<doom-ui>>

 :editor
 <<doom-editor>>

 :emacs
 <<doom-emacs>>

 :term
 <<doom-term>>

 :checkers
 <<doom-checkers>>

 :tools
 <<doom-tools>>

 :os
 <<doom-os>>

 :lang
 <<doom-lang>>

 :email
 <<doom-email>>

 :app
 <<doom-app>>

 :config
 <<doom-config>>)
#+END_SRC
** Input
#+NAME: doom-input
#+BEGIN_SRC emacs-lisp
;;chinese
;;japanese
;;layout            ; auie,ctsrnm is the superior home row
#+END_SRC
** Completion
#+NAME: doom-completion
#+BEGIN_SRC emacs-lisp
(company           ; the ultimate code completion backend
 +childframe)       ; ... when your children are better than you
;;helm              ; the *other* search engine for love and life
;;ido               ; the other *other* search engine...
;(ivy               ; a search engine for love and life
; +prescient         ; I know what I want(ed)
; +icons)           ; ...icons are nice
(vertico
 +icons)
#+END_SRC
** UI
#+NAME: doom-ui
#+BEGIN_SRC emacs-lisp
;;deft              ; notational velocity for Emacs
doom              ; what makes DOOM look the way it does
doom-dashboard    ; a nifty splash screen for Emacs
;;doom-quit         ; DOOM quit-message prompts when you quit Emacs
;;fill-column       ; a `fill-column' indicator
hl-todo           ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW
;;hydra
;;indent-guides     ; highlighted indent columns
;;(ligatures +extra)         ; ligatures and symbols to make your code pretty again
;;minimap           ; show a map of the code on the side
modeline          ; snazzy, Atom-inspired modeline, plus API
;;nav-flash         ; blink cursor line after big motions
;;neotree           ; a project drawer, like NERDTree for vim
ophints           ; highlight the region an operation acts on
(popup            ; tame sudden yet inevitable temporary windows
 +all             ; catch all popups that start with an asterisk
 +defaults)       ; default popup rules
;;tabs              ; a tab bar for Emacs
treemacs          ; a project drawer, like neotree but cooler
unicode           ; extended unicode support for various languages
;;vc-gutter         ; vcs diff in the fringe
vi-tilde-fringe   ; fringe tildes to mark beyond EOB
(window-select     ; visually switch windows
 +numbers)
workspaces        ; tab emulation, persistence & separate workspaces
;;zen               ; distraction-free coding or writing
#+END_SRC
** Editor
#+NAME: doom-editor
#+BEGIN_SRC emacs-lisp
(evil +everywhere); come to the dark side, we have cookies
file-templates    ; auto-snippets for empty files
fold              ; (nigh) universal code folding
(format +onsave)  ; automated prettiness
;;god               ; run Emacs commands without modifier keys
;;lispy             ; vim for lisp, for people who don't like vim
;;multiple-cursors  ; editing in many places at once
;;objed             ; text object editing for the innocent
;;parinfer          ; turn lisp into python, sort of
rotate-text       ; cycle region at point between text candidates
snippets          ; my elves. They type so I don't have to
;;word-wrap         ; soft wrapping with language-aware indent
#+END_SRC
** Emacs
#+NAME: doom-emacs
#+BEGIN_SRC emacs-lisp
(dired             ; making dired pretty [functional]
 +icons)          ; icons are nice
electric          ; smarter, keyword-based electric-indent
;;ibuffer         ; interactive buffer management
(undo              ; persistent, smarter undo for your inevitable mistakes
 +tree)
vc                ; version-control and Emacs, sitting in a tree
#+END_SRC
** Term
#+NAME: doom-term
#+BEGIN_SRC emacs-lisp
;;eshell            ; the elisp shell that works everywhere
;;shell             ; simple shell REPL for Emacs
;;term              ; basic terminal emulator for Emacs
;;vterm             ; the best terminal emulation in Emacs
#+END_SRC
** Checkers
#+NAME: doom-checkers
#+BEGIN_SRC emacs-lisp
syntax              ; tasing you for every semicolon you forget
;;(spell             ; tasing you for misspelling mispelling
;; +flyspell               ; Tase you a bit more accurately than spell-fu
;; +aspell)           ; The classic backend
;;(grammar           ; tasing grammar mistake every you make
;; +langtool
;; +writegood-mode)
#+END_SRC
** Tools
#+NAME: doom-tools
#+BEGIN_SRC emacs-lisp
;;ansible
;;(debugger +lsp)          ; FIXME stepping through code, to help you add bugs
;;direnv
;;docker
;;editorconfig      ; let someone else argue about tabs vs spaces
;;ein               ; tame Jupyter notebooks with emacs
(eval +overlay)     ; run code, run (also, repls)
;;gist              ; interacting with github gists
lookup              ; navigate your code and its documentation
;;lsp
(magit             ; a git porcelain for Emacs
 +forge)            ; Emacs > Github
make              ; run make tasks from Emacs
;;pass              ; password manager for nerds
pdf               ; pdf enhancements
;;prodigy           ; FIXME managing external services & code builders
;;rgb               ; creating color strings
;;taskrunner        ; taskrunner for all your projects
;;terraform         ; infrastructure as code
;;tmux              ; an API for interacting with tmux
;;upload            ; map local to remote projects via ssh/ftp
#+END_SRC
** OS
#+NAME: doom-os
#+BEGIN_SRC emacs-lisp
(:if IS-MAC macos)  ; improve compatibility with macOS
tty               ; improve the terminal Emacs experience
#+END_SRC
** Lang
#+NAME: doom-lang
#+BEGIN_SRC emacs-lisp
;;agda              ; types of types of types of types...
;;(cc                ; C/C++/Obj-C madness
;; +lsp)
;;clojure           ; java with a lisp
;;common-lisp       ; if you've seen one lisp, you've seen them all
;;coq               ; proofs-as-programs
;;crystal           ; ruby at the speed of c
;;csharp            ; unity, .NET, and mono shenanigans
data              ; config/data formats
;;(dart +flutter)   ; paint ui and not much else
;;elixir            ; erlang done right
;;elm               ; care for a cup of TEA?
emacs-lisp        ; drown in parentheses
;;erlang            ; an elegant language for a more civilized age
;;ess               ; emacs speaks statistics
;;faust             ; dsp, but you get to keep your soul
;;fsharp            ; ML stands for Microsoft's Language
;;fstar             ; (dependent) types and (monadic) effects and Z3
;;gdscript          ; the language you waited for
;;(go +lsp)         ; the hipster dialect
;;(haskell +dante)  ; a language that's lazier than I am
;;hy                ; readability of scheme w/ speed of python
;;idris             ;
json              ; At least it ain't XML
;;(java +meghanada) ; the poster child for carpal tunnel syndrome
;;javascript        ; all(hope(abandon(ye(who(enter(here))))))
;;julia             ; a better, faster MATLAB
;;kotlin            ; a better, slicker Java(Script)
(latex              ; writing papers in Emacs has never been so fun
 +latexmk           ; what else would you use?
                                        ; +cdlatex           ; quick math (symbols)
 +fold)             ; fold the clutter away
;;lean
;;factor
;;ledger            ; an accounting system in Emacs
;;lua               ; one-based indices? one-based indices
markdown          ; writing docs for people to ignore
;;nim               ; python + lisp at the speed of c
;;nix               ; I hereby declare "nix geht mehr!"
;;ocaml             ; an objective camel
(org               ; organize your plain life in plain text
 +pretty            ; yessss, my pretties (nice unicode symbols)
 +dragndrop         ; drag and drop images into Org buffers
 ;;+hugo              ; Org+Hugo for blogging
 ;;+juptyer           ; Juptyer support for babel
 +pandoc            ; export-with-pandoc functionality
 +roam2)             ; wander around notes
 ;;+present           ; org-mode for presentations
;; +gnuplot)           ; beautiful plotting made easy
;;php               ; perl's insecure younger brother
;;plantuml          ; diagrams for confusing people more
;;purescript        ; javascript, but functional
;;(python            ; beautiful is better than ugly
;; +lsp
;; +pyright)
;;qt                ; the 'cutest' gui framework ever
;;racket            ; a DSL for DSLs
;;raku              ; the artist formerly known as perl6
;;rest              ; Emacs as a REST client
;;rst               ; ReST in peace
;;(ruby +rails)     ; 1.step {|i| p "Ruby is #{i.even? ? 'love' : 'life'}"}
;;(rust              ; Fe2O3.unwrap().unwrap().unwrap().unwrap()
;; +lsp)
;;scala             ; java, but good
;;scheme            ; a fully conniving family of lisps
sh                ; she sells {ba,z,fi}sh shells on the C xor
;;sml
;;solidity          ; do you need a blockchain? No.
;;swift             ; who asked for emoji variables?
;;terra             ; Earth and Moon in alignment for performance.
;;web               ; the tubes
yaml              ; JSON, but readable
#+END_SRC
** Email
#+NAME: doom-email
#+BEGIN_SRC emacs-lisp
;;(mu4e +org)
;;notmuch
;;(wanderlust +gmail)
#+END_SRC
** App
#+NAME: doom-app
#+BEGIN_SRC emacs-lisp
;;calendar
;;irc               ; how neckbeards socialize
(rss +org)        ; emacs as an RSS reader
;;twitter           ; twitter client https://twitter.com/vnought
#+END_SRC
** Config
#+NAME: doom-config
#+BEGIN_SRC emacs-lisp
literate
(default +bindings +smartparens)
#+END_SRC
* General Configuration
Some functionality uses this to identify you, e.g. GPG configuration, email clients, file templates and snippets.
#+BEGIN_SRC emacs-lisp

#+END_SRC
** Daemon
Setting up the Emacs daemon makes startup of new instances almost instant as well as enables server-like functionality (monitoring for changes, etc.). Most (all) is stolen from Tecosaur.

First we have to start the server. Systemd lets us start a user service on login.
#+name: emacsclient service
#+begin_src systemd :tangle ~/.config/systemd/user/emacs.service :mkdirp yes
[Unit]
Description=Emacs server daemon
Documentation=info:emacs man:emacs(1) https://gnu.org/software/emacs/

[Service]
Type=forking
ExecStart=sh -c 'emacs --daemon; emacsclient -c --eval "(delete-frame)"'
ExecStop=/usr/bin/emacsclient --no-wait --eval "(progn (setq kill-emacs-hook nil) (kill emacs))"
Restart=on-failure

[Install]
WantedBy=default.target
#+end_src

Enable the Emacs daemon on startup with
#+begin_src sh
systemctl --user enable emacs.service
#+end_src

We'd also like to be able to use this as the default application for test files among other things. The Free Desktop Organization has created a standard for this: desktop files.
#+begin_src conf :tangle ~/.local/share/applications/emacs-client.desktop :mkdirp yes
[Desktop Entry]
Name=Emacs client
GenericName=Text Editor
Comment=A flexible platform for end-user applications
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
Exec=emacsclient -create-frame --alternate-editor="" --no-wait %F
Icon=emacs
Type=Application
Terminal=false
Categories=TextEditor;Utility;
StartupWMClass=Emacs
Keywords=Text;Editor;
X-KDE-StartupNotify=false
#+end_src

src_sh{emacsclient -c -a '' <filename>} is a lot to type whenever we want to open a file from terminal. src_sh{$EDITOR <filename>} isn't /that/ much better. Let's use a shell script wrapping Emacsclient for one-key access from the terminal.
#+name: e
#+begin_src shell :tangle ~/.local/bin/e :mkdirp yes :tangle-mode (identity #o755) :comments no
#!/usr/bin/env bash
force_tty=false
force_wait=false
stdin_mode=""

args=()

while :; do
    case "$1" in
        -t | -nw | --tty)
            force_tty=true
            shift ;;
        -w | --wait)
            force_wait=true
            shift ;;
        -m | --mode)
            stdin_mode=" ($2-mode)"
            shift 2 ;;
        -h | --help)
            echo -e "\033[1mUsage: e [-t] [-m MODE] [OPTIONS] FILE [-]\033[0m

Emacs client convenience wrapper.

\033[1mOptions:\033[0m
\033[0;34m-h, --help\033[0m            Show this message
\033[0;34m-t, -nw, --tty\033[0m        Force terminal mode
\033[0;34m-w, --wait\033[0m            Don't supply \033[0;34m--no-wait\033[0m to graphical emacsclient
\033[0;34m-\033[0m                     Take \033[0;33mstdin\033[0m (when last argument)
\033[0;34m-m MODE, --mode MODE\033[0m  Mode to open \033[0;33mstdin\033[0m with

Run \033[0;32memacsclient --help\033[0m to see help for the emacsclient."
            exit 0 ;;
        --*=*)
            set -- "$@" "${1%%=*}" "${1#*=}"
            shift ;;
        *)
            if [ "$#" = 0 ]; then
                break; fi
            args+=("$1")
            shift ;;
    esac
done

if [ ! "${#args[*]}" = 0 ] && [ "${args[-1]}" = "-" ]; then
    unset 'args[-1]'
    TMP="$(mktemp /tmp/emacsstdin-XXX)"
    cat > "$TMP"
    args+=(--eval "(let ((b (generate-new-buffer \"*stdin*\"))) (switch-to-buffer b) (insert-file-contents \"$TMP\") (delete-file \"$TMP\")${stdin_mode})")
fi

if [ -z "$DISPLAY" ] || $force_tty; then
    # detect terminals with sneaky 24-bit support
    if { [ "$COLORTERM" = truecolor ] || [ "$COLORTERM" = 24bit ]; } \
        && [ "$(tput colors 2>/dev/null)" -lt 257 ]; then
        if echo "$TERM" | grep -q "^\w\+-[0-9]"; then
            termstub="${TERM%%-*}"; else
            termstub="${TERM#*-}"; fi
        if infocmp "$termstub-direct" >/dev/null 2>&1; then
            TERM="$termstub-direct"; else
            TERM="xterm-direct"; fi # should be fairly safe
    fi
    emacsclient --tty -create-frame --alternate-editor="" "${args[@]}"
else
    if ! $force_wait; then
        args+=(--no-wait); fi
    emacsclient -create-frame --alternate-editor="" "${args[@]}"
fi
#+end_src
* Visuals
Doom exposes five (optional) variables for controlling fonts in Doom. Here
are the three important ones:
+ ~doom-font~
+ ~doom-variable-pitch-font~
+ ~doom-big-font~ -- used for ~doom-big-font-mode~; use this for presentations or streaming.
They all accept either a font-spec, font string ("Input Mono-12"), or xlfd font string.
#+BEGIN_SRC emacs-lisp

#+END_SRC

This determines the style of line numbers in effect. Relative line numbers are good for jumping around without having to do mental math. The visual setting displays visual lines and thus correctly handles collapsed Org headings.
#+BEGIN_SRC emacs-lisp
#+END_SRC

Always start Emacs maximized.
#+BEGIN_SRC emacs-lisp
#+END_SRC

Weeeeeeeeee
#+begin_src emacs-lisp
#+end_src
** Modeline
[[https://github.com/seagle0128/doom-modeline][Doom Modeline]], a fancy and fast modeline integrated with [[github:hlissner/doom-emacs][Doom Emacs]].

Starting Emacs with =fullboth= seems to disable the dock in KDE. Having a battery indicator and clock becomes a nice change. That and disabling some visual clutter makes for a nice modeline.
#+BEGIN_SRC emacs-lisp
#+END_SRC
Credit [[github:tecosaur][@tecosaur]]: =LF UTF-8= is the default file encoding, and thus not worth noting in the modeline. So, let’s conditionally hide it.
#+begin_src emacs-lisp
#+end_src
* Spell
[[https:www.gnu.org/software/emacs/manual/html_node/emacs/Spelling.html][Flyspell]], a wrapper around several spell-checking engines for Emacs.

#+BEGIN_SRC emacs-lisp
(use-package! flyspell
  :disabled
  :defer t)
#+END_SRC

** Flyspell-Lazy
~flyspell-lazy~ is a tad too lazy for my tastes. Let's speed it up.
#+BEGIN_SRC emacs-lisp
#+END_SRC
* \LaTeX
 #+BEGIN_SRC emacs-lisp
 #+END_SRC
** CDLaTeX
[[github:cdominik/cdlatex][CDLaTeX]], a mode for quick insertion of mathematical symbols in \LaTeX.
#+BEGIN_SRC emacs-lisp
#+END_SRC
* Snippets
** Yasnippet
[[github:joaotavora/yasnippet][Yasnippet]], a template system for Emacs.

Yasnippet doesn't allow nested snippet expansion by default. Let's fix that.
#+BEGIN_SRC emacs-lisp
(use-package! yasnippet
  :custom
  (yas-triggers-in-field t))
#+END_SRC
** Auto-Activating-Snippets
[[github:ymarco/auto-activating-snippets][Auto-activating-snippets]], an engine for automatically expanding snippets based on keystrokes.

I prefer this package to Yasnippet's half-baked auto-expansion ([[https:github.com/joaotavora/yasnippet/pull/1048/][pull request]]). Time will tell if this remains true.
#+BEGIN_SRC emacs-lisp :tangle packages.el

#+END_SRC

#+BEGIN_SRC emacs-lisp
#+END_SRC
** \LaTeX​-Auto-Activating-Snippets
[[github:tecosaur][@tecosaur]] and [[github:ymarco][@ymarco]] have created a very nice auto-snippets package for \LaTeX. Let's use them, shall we?
#+BEGIN_SRC emacs-lisp :tangle packages.el

#+END_SRC
...and use them.
#+BEGIN_SRC emacs-lisp
#+END_SRC
* Smartparens
[[github:Fuco1/smartparens][Smartparens]], an intelligent pair-insertion package.
#+BEGIN_SRC emacs-lisp :noweb yes
(use-package! smartparens-config
  :config
  <<smartparens-latex-mode>>
  <<smartparens-org-mode>>)
#+END_SRC

** \LaTeX
#+NAME: smartparens-latex-mode
#+BEGIN_SRC emacs-lisp :tangle no
#+END_SRC
** Org
I want Org's markup characters to come in pairs but only when it "makes sense". "Makes sense" is defined as the point is in none of the following:
- a source block
- verbatim emphasis
- a drawer (like :PROPERTIES:)
- a \LaTeX fragment.

There is a zero-width space character before and after the opening and closing {=* / + _ ~ \==} characters respectively. This allows emphasis to apply to parts of words. Try typing them all. It's kind of fun.
#+BEGIN_EXAMPLE
cat​*tag*​cat  cat​/tag/​cat  cat​+tag+​cat  cat​_tag_​cat  cat​~tag~​cat  cat​=tag=​cat  cat<<tag>>cat
#+END_EXAMPLE
#+NAME: smartparens-org-mode
#+BEGIN_SRC emacs-lisp :tangle no
(defun sifus/sp-org-in-src-block-p (_id _action _context) (org-in-src-block-p))
(defun sifus/sp-org-in-verbatim-emphasis (_id _action _context) (org-in-verbatim-emphasis))
(defun sifus/sp-org-inside-LaTeX-fragment-p (_id _action _context) (org-inside-LaTeX-fragment-p))
(defun sifus/sp-org-in-drawer (_id _action _context)
  (let ((element (car (org-element-property :parent (org-element-at-point)))))
    (or (equal element 'drawer ) (equal element 'property-drawer))))
(defun sifus/sp-org-in-hashplus (_id _action _context)
  (and (equal (char-after (line-beginning-position)) ?#)
       (equal (char-after (+ (line-beginning-position) 1)) ?+)))
(sp-with-modes 'org-mode
  (sp-local-pair "​*" "*​" :trigger "*" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "​/" "/​" :trigger "//" :unless '(sifus/sp-org-in-src-block-p
                                                 sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                 sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "​+" "+​" :trigger "+" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "​_" "_​" :trigger "_" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "​~" "~​" :trigger "~" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "​=" "=​" :trigger "=" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis
                                                sifus/sp-org-in-drawer
                                                sifus/sp-org-in-hashplus
                                                sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "<<" ">>" :trigger "<<" :unless '(sifus/sp-org-in-verbatim-emphasis
                                                   sifus/sp-org-in-hashplus
                                                   sifus/sp-org-in-drawer
                                                   sifus/sp-org-inside-LaTeX-fragment-p))
  (sp-local-pair "$" "$" :trigger "$" :unless '(sifus/sp-org-in-src-block-p
                                                sifus/sp-org-in-verbatim-emphasis))
  (sp-local-pair "'" "'" :trigger "'" :unless '((lambda (_id _action _context) (equal (org-eldoc-get-src-lang) "emacs-lisp")))))
#+END_SRC
* PDF
[[github:politza/pdf-tools][PDF Tools]], a PDF library for Emacs.

We use dark mode in this house. PDF Tools can trim pages so margins are out of view. Do that automatically.
#+BEGIN_SRC emacs-lisp
#+END_SRC
* Org
[[https:orgmode.org/][Org-mode]], a lightweight markup language and powerful tool for note-taking and text-based organization.
** General Configuration
The inability to insert emphasis markup in the middle of words (e.g. =mid*dl*e=) is a notable shortcoming of Org markup. My preferred workaround is adding a zero-width space character via ~insert-char~, by default bound to =C-x 8 RET=. There exist [[https:stackoverflow.com/questions/1218238/how-to-make-part-of-a-word-bold-in-org-mode][other workarounds]] but that one in particular started making some Org links italic.

#+BEGIN_SRC emacs-lisp
#+END_SRC
#+begin_src emacs-lisp
#+end_src

** Story Archiving
I like to go on [[https://old.reddit.com/r/WritingPrompts][r/WritingPrompts]] and [[https://old.reddit.com/r/HFY][r/HFY]] for some light reading. Many of these authors like to self-publish their stories on Amazon as ebooks. Unfortunately, Amazon has a monopoly over the ebook market and forces authors to remove whatever they want to publish from the internet. This naturally puts a bit of a snag in my reading. Because I don't want to support these monopolistic practices (or Amazon in general really), I archive whatever stories I'm interested. (For those worried that the author won't get paid if I archive instead of buy the book, fear not! If a story I enjoy gets published, I always reach out to the author to send money directly.) My archival strategy is as follows:
1) Pull the original Markdown.
2) Convert it to the vastly superior Org format for easy folding by chapters.
3) Strip out some pesky raw HTML characters and other unwanted characters.
4) Manually format as necessary.

I pull the Markdown with an HTTP request via Python:
#+begin_src python :tangle "~/.local/bin/reddit-to-md" :tangle-mode (identity #o755) :mkdirp yes
#!/usr/bin/env python3

import sys
import requests
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Download Markdown of a Reddit post or comment."
    )
    parser.add_argument("URL")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--post", action="store_true", default=True)
    group.add_argument("--comment", action="store_true")
    args = parser.parse_args()

    url = args.URL
    headers = {"User-agent": "Python:test"}
    post_json = requests.get(url + ".json", headers=headers)
    if args.comment:
        try:
            # Comment
            print(post_json.json()[1]["data"]["children"][0]["data"]["body"])
        except Exception as e:
            print(f"Couldn't pull URL {url}: {e}")
            exit(1)
    else:
        try:
            # Post
            print(post_json.json()[0]["data"]["children"][0]["data"]["selftext"])
        except Exception as e:
            print(f"Couldn't pull URL {url}: {e}")
            exit(1)
#+end_src
Then I /would/ pipe it to Pandoc, but Pandoc always adds some unnecessary property drawers. I strip out the offending attributes with Lua and use it as a Pandoc filter:
#+begin_src lua :tangle "~/.local/bin/remove-header-attr.lua" :mkdirp yes
function Header (header)
  return pandoc.Header(header.level, header.content, pandoc.Attr())
end
#+end_src
Then append it to an Org file. The entire call looks a little like this:
#+begin_src sh :tangle no
reddit-to-md 'https://old.reddit.com/r/WritingPrompts/<something>' | \
    pandoc --wrap=none -t org --lua-filter="$HOME/bin/remove-header-attr.lua" | \
    sed -e 's/&#x200B/​/g' -e 's/&nbsp;/ /g' >> 'Story File.org'
#+end_src
...or maybe like this if I have a list of chapters:
#+begin_src bash :tangle no
#!/usr/bin/env bash
set -euo pipefail

chapter=$((1))
story='Story file.org'
while read -r f; do
    echo "* Chapter $chapter" >> "$story" && \
        reddit-to-md "$f" | \
        pandoc --wrap=none -t org --lua-filter="$HOME/.local/bin/remove-header-attr.lua" | \
        sed -e 's/&#x200B/​/g' -e 's/&nbsp;/ /g' >> "$story"
        chapter=$(( $chapter + 1 )) && \
        cat "$story"
done <'/tmp/chapters.txt'
#+end_src
...which can (usually) be retrieved with something like this:
#+begin_src bash :tangle no
#!/usr/bin/env bash
set -euo pipefail

url='https://reddit.com/r/HFY/comments/<something>'
CMD() {
    reddit-to-md "$url" | \
        grep '\[Next\]' | \
        head -n1 | \
        sed -E 's;.*\[Next\]\((.*reddit.com/r/.+/comments/.+/.+/).*\).*;\1;g'
}

CHAPTERS='/tmp/chapters.txt'
echo "$url" >> "$CHAPTERS"
next="$(CMD)"
while [ "$?" -eq 0 ]; do
    echo "$next"
    echo "$next" >> "$CHAPTERS"
    url="$next"
    next="$(CMD)"
done
#+end_src
** Org-Fragtog
[[github:io12/org-fragtog][Org-fragtog]], a package to automatically toggle preview of \LaTeX fragments in org-mode
#+BEGIN_SRC emacs-lisp :tangle packages.el

#+END_SRC
#+BEGIN_SRC emacs-lisp
(use-package! org-fragtog
  :after org
  :hook (org-mode . org-fragtog-mode))
#+END_SRC
** Toc-Org
[[https://github.com/snosov1/toc-org][Toc-org]], an Org-mode table of contents that updates on save, creating an always up-to-date table of contents.
#+BEGIN_SRC emacs-lisp :tangle packages.el

#+END_SRC
** Agenda
#+begin_src emacs-lisp
#+end_src

I want desktop notifications for agenda appointments. Org-agenda provides ​~org-agenda-to-appt~​ but it doesn't do desktop notifications. We can manipulate the called function to run an external command. It's kind of gross though.
#+begin_src emacs-lisp
#+end_src
* Projectile
[[github:bbatsov/projectile][Projectile]], a project interaction library for Emacs.

Originally, I tried to add =~/.doom.d/= to my projects but that isn't necessary; =SPC f P= is globally available and opens ~doom-private-dir~.
#+BEGIN_SRC emacs-lisp
#+END_SRC
* Evil
[[github:emacs-evil/evil][Evil]], an *e*​xtensible *vi* *l*​ayer for Emacs.
* Rainbow Delimiters
[[https://github.com/Fanael/rainbow-delimiters][Rainbow Delimiters]], a mode to highlight matching pairs according to their depth.

A maximum depth of three isn't enough. Those are rookie numbers. You gotta pump those number up.
#+BEGIN_SRC emacs-lisp

#+END_SRC
* RSS
We set up RSS to continue our goal of never leaving Emacs and Org-mode. We use Doom's ​~(:app rss +org)~​ module to setup [[https://github.com/skeeto/elfeed][elfeed]] for an Emacs-based RSS reader and [[https://github.com/remyhonig/elfeed-org][elfeed-org]] to allow Org-mode to keep track of the feeds.
#+begin_src emacs-lisp
#+end_src

I want to keep my ​=feeds.org=​ file with the rest of my Emacs configuration i.e. ​~doom-private-dir/feeds.org~​ but it defaults to ​~org-directory/elfeed.org~​.
#+begin_src emacs-lisp

#+end_src

I like to keep my feeds file encrypted when I push it to a Git repository. This works well except when I want to edit the feeds. Elfeed-org's feed editing is limited which forces me to edit feeds elsewhere. My solution is to edit the feeds in ​=feeds.org=​, then tangle the source blocks out. Unfortunately, Org doesn't support tangling multiple files to a single output file. My solution is to include ​=feeds.org=​ into this file with the ​~#+INCLUDE~​ directive then tangle it from here. This leads to another roadblock; Org doesn't resolve includes before tangling. That can be sidestepped with this disgusting advice.
#+begin_src emacs-lisp
(defadvice! sifus/org-include-then-tangle (orig-fn &rest args)
  "Expand included Org files before tangling"
  :around #'org-babel-tangle
  (require 'ox)
  (org-export-with-buffer-copy
   (org-export-expand-include-keyword)
   (apply orig-fn args)))

  ;; (let ((org-babel-pre-tangle-hook (remove #'save-buffer org-babel-pre-tangle-hook)))
  ;;   (save-buffer)
  ;;   (org-export-expand-include-keyword)
  ;;   (apply orig-fn args)
  ;;   (revert-buffer nil t)))
#+end_src

Now include the feeds file here.

Now we do some editing of the feeds. We make a subset feed of this one:
#+begin_src emacs-lisp

#+end_src

The ability to open some RSS links in other applications is very useful. I want to open videos in MPV. We use the snippet by [[https:ambrevar.xyz][Ambrevar]] in [[https://github.com/skeeto/elfeed/issues/267][this Github issue]].
#+begin_src emacs-lisp
#+end_src
* Auth-source
Let's let Emacs read some credentials. We use the ​=auth-source=​ library included with Emacs. ​=auth-source=​ can read from serveral backends. We'll make use of the ​=secrets=​ backend to read from KeePassXC, which provides an implementation of the Secret Service API.

Now we just need to enable the backend.
#+begin_src emacs-lisp

#+end_src
* Popup
#+begin_src emacs-lisp
#+end_src
* Media Journal
I like keeping a log of substantial media I've consumed (books, television, movies, etc.). It helps me keep track of what I've watched, what I thought about it, and information about it. I highly recommend everyone keep a similar log. It's particularly interesting when rewatching/rereading media. I'd bet any amount of money that the set of scenes/quotes/etc. that stuck out to you in the first viewing isn't equal to the set of scenes/quotes/etc. that stuck out to you in the second viewing. Further, I'd bet any amount of money again that the intersection between those sets is nonempty.
** Books
I keep track of my (e)books in [[https://calibre-ebook.com/][Calibre]]. Having an Emacs interface will be useful.
#+begin_src emacs-lisp :tangle packages.el

#+end_src

Now we need to tell it where my Calibre library is and make it /evil/.
#+begin_src emacs-lisp

#+end_src
** TV and Movies
We can use [[https://github.com/isamert/orgmdb.el][OrgMDB]] to query the Open Movie Database for movie and TV metadata.
#+begin_src emacs-lisp :tangle packages.el

#+end_src
It requires an API key, which we can securely provide via [[https://www.gnu.org/software/emacs/manual/html_mono/auth.html][auth-source]].
#+begin_src emacs-lisp

#+end_src
* Chezmoi
#+begin_src emacs-lisp :tangle packages.el
(package! chezmoi :recipe (:host github :repo "tuh8888/chezmoi.el")
  :pin "73ea4cac4453685f59213b0bbc55ea645eac4a5e")
#+end_src
* Magit
#+begin_src emacs-lisp :tangle packages.el :tangle no
(package! libegit2
  :recipe (:local-repo "~/projects/libegit2"
           :pre-build (("mkdir" "-p" "build")
                       ("sh" "-c" "cmake -B build -DUSE_SYSTEM_LIBGIT2=1 .")
                       ("make" "-C" "build"))
           :files ("libgit.el" "build")))
#+end_src

#+begin_src emacs-lisp :tangle no
(use-package! libegit2
  :init (setq libgit--module-file
              (expand-file-name "libegit2.so"
                                (concat straight-base-dir straight-build-dir "libegit2"))))
#+end_src
