;; -*- lexical-binding: t; -*-

(setq user-full-name "Sifus"
      user-mail-address "sifus@sifus.org")

; Visuals
(setq doom-font (font-spec :family "JetBrains Mono" :size 24)
      doom-big-font (font-spec :family "JetBrains Mono" :size 36)
      doom-variable-pitch-font (font-spec :family "Overpass" :size 24)
      doom-serif-font (font-spec :family "IBM Plex Mono" :weight 'light)
      ; Themes I liked:
      ; acario-dark ayu-dark ayu-mirage gruvbox henna monokai monokai-classic
      ; monokai-pro moonlight old-hope palenight snazzy tomorrow-night dracula
      doom-theme 'doom-dracula
      ; Visual handles collapsed Org headings better than relative
      display-line-numbers-type 'visual)
; Always start maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(use-package! rainbow-delimiters
  :config
  (setq rainbow-delimiters-max-face-count 6))

; TODO Spell
;; (use-package! flyspell-lazy
;;   :disabled
;;   :after flyspell
;;   :custom
;;   (flyspell-lazy-window-idle-seconds 2)
;;   (flyspell-lazy-use-flyspell-word t)
;;   (flyspell-lazy-idle-seconds .25))

; TODO Latex
; TODO investigate what's needed
;; set +latex-viewers instead
;; (use-package! tex
;;   :custom
;;   (TeX-auto-save t)
;;   (TeX-parse-self t)
;;   (TeX-save-query nil)
;;   (TeX-master nil)
;;   (TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
;;   ;; to use pdfview with auctex
;;   (TeX-view-program-selection '((output-pdf "PDF Tools")
;;                                 (output-pdf "pdf-tools")
;;                                 (output-pdf "zathura")
;;                                 (output-pdf "Okular")
;;                                 (output-pdf "preview-pane")
;;                                 (output-dvi "xdvi")
;;                                 (output-html "xdg-open")))
;;   :hook
;;   (LaTeX-mode . (lambda ()
;;                   (turn-on-reftex)
;;                   (setq reftex-plug-into-AUCTeX t)
;;                   (reftex-isearch-minor-mode)
;;                   (setq TeX-PDF-mode t)
;;                   (setq TeX-source-correlate-method 'synctex)
;;                   (setq TeX-source-correlate-start-server t)
;;                   (when (featurep! :lang latex +fold)
;;                     (add-hook 'evil-insert-state-exit-hook 'TeX-fold-paragraph)))))

;; (use-package! cdlatex
;;   :disabled
;;   :custom
;;   (cdlatex-math-symbol-prefix nil)
;;   :hook
;;   ((latex-mode LaTeX-mode TeX-mode) . 'turn-on-cdlatex))

; Snippets
(use-package! yasnippet
  :config
  (setq yas-triggers-in-field t)) ; Nested snippet expansion

(use-package! auto-activating-snippets
  :config
  :hook ((LaTeX-mode org-mode) . aas-activate-for-major-mode))

(use-package! laas
  :hook (LaTeX-mode . laas-mode)
;  :after (latex auto-activating-snippets yasnippet)
  :config
  (after! laas
    (aas-set-snippets 'laas-mode
                      :cond #'texmathp
                      ;; "(" (lambda ()
                      ;;       (interactive)
                      ;;       (yas-expand-snippet "\\left($1\\right)$0"))
                      "inv" "^{-1}"
                      "OO" "\\circ{}"
                      "'=" "\\cong ")))

; Parentheses in math mode are being taken care of by auto-activating-snippets
(sp-with-modes '(tex-mode plain-tex-mode latex-mode LaTeX-mode)
  (sp-local-pair "(" ")" :trigger "(" :unless '(sp-in-math-p))
  (sp-local-pair "\\left(" "\\right)" :when '(sp-in-math-p)))

(use-package! pdf-tools
  :config
  (pdf-tools-install)
  (dolist (mode '(pdf-view-midnight-minor-mode pdf-view-auto-slice-minor-mode))
    (add-to-list 'pdf-tools-enabled-modes mode)))

; Org
(use-package! org
  :init
  (setq org-directory "~/Documents/org")
  :config
  (setq org-log-done 'done
        org-roam-directory (concat org-directory "/roam")
        org-cite-global-bibliography `(,(concat org-directory "/zotero.bib")))

  ; TODO needed?
  ;; (after! org
  ;;   (plist-put org-format-latex-options :background 'default)
  ;;   (org-roam-db-autosync-mode))
  )

; TODO decide if I want this
;; (use-package! org-fragtog
;;   :after org
;;   :hook (org-mode . org-fragtog-mode))


; TODO Org-agenda
;; (after! org
;;   (setq org-agenda-files `(,(concat org-directory "/agenda"))
;;         org-deadline-warning-days 10
;;         appt-display-format 'window
;;         appt-disp-window-function '(lambda (time-until app-time text)
;;                                      (save-window-excursion
;;                                        (shell-command (format
;;                                                        "notify-send '%s minutes until %s' 'Appointment is at %s'"
;;                                                        time-until text app-time)
;;                                                       nil nil)))
;;         appt-delete-window-function 'appt-delete-window
;;         appt-display-diary nil
;;         appt-message-warning-time 10
;;         appt-display-interval 10)
;;   (add-hook! 'org-finalize-agenda-hook (lambda ()
;;                                          (setq appt-time-msg-list nil)
;;                                          (org-agenda-to-appt)
;;                                          (appt-activate 1))))

; Projectile
(use-package! projectile
  :config
  (setq projectile-auto-discover t
        projectile-enable-caching t
        projectile-project-search-path '("~/projects/" "~/Documents/classes"))
  )
  ; TODO see whats needed
  ;; (dolist (ext '(".eln" ".aux" ".synctex.gz" ".fls" ".fdb_latexmk"))
  ;;   (add-to-list 'projectile-globally-ignored-file-suffixes ext))
  ;; (dolist (dir '("*ltximg" "*.auctex-auto" "~/.cache" "~/.local" "*auto"))
  ;;   (add-to-list 'projectile-globally-ignored-directories dir)))

; RSS
(defun sifus/elfeed-index-last-mod-time ()
  (file-attribute-modification-time
   (file-attributes (expand-file-name (concat elfeed-db-directory "/index")))))

(defun sifus/elfeed-update ()
  "Load any changes to the Elfeed database before updating it"
  (interactive)
  (when (time-less-p sifus/elfeed-last-refresh
                     (sifus/elfeed-index-last-mod-time))
    (message "Loaded changed database")
    (elfeed-db-load)
    (elfeed-search-update--force))
  (elfeed-update)
  (setq sifus/elfeed-last-refresh (current-time)))

(use-package! elfeed
  :config
  (setq elfeed-db-directory (expand-file-name "~/Documents/elfeed")
        elfeed-search-filter "@4-week-ago +unread -junk"
        sifus/elfeed-last-refresh '(0 0 0 0))
  (add-hook! 'elfeed-search-mode-hook 'sifus/elfeed-update)
  (elfeed-org))

(use-package! elfeed-org
  :init
  (setq rmh-elfeed-org-files `(,(expand-file-name (concat doom-private-dir "/feeds.org")))))

(map! :map elfeed-search-mode-map
      :after elfeed-search
      :n "e" #'sifus/elfeed-update)
(map! :map elfeed-show-mode-map
      :after elfeed-show)

(after! elfeed
  (load! "feeds.el"))

;; Open some links in other apps

(defvar mpv-socket "/tmp/mpvsocket")

(defun browse-url-mpv (url &optional single)
  (let ((buffer "*mpv*"))
    (make-network-process
     :service (expand-file-name mpv-socket)
     :family 'local
     :name "mpv-ipc"
     :buffer buffer)
    (with-current-buffer buffer
      (process-send-string nil
                           (format "{ command: [ \"loadfile\", \"%s\", \"append-play\" ] }\n" url))
      (accept-process-output)
      (delete-process buffer)
      (buffer-string))))

(defun sifus/elfeed-play-with-mpv ()
  "Play entry link with mpv."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single))))
    (browse-url-mpv (elfeed-entry-link entry))))

(defvar sifus/elfeed-visit-patterns
  '(("//[^/]*youtu\\.?be" . sifus/elfeed-play-with-mpv))
  "List of (regexps . function) to match against elfeed entry link to know
whether how to visit the link.")

(defun sifus/elfeed-visit-maybe-external ()
  "Visit with external function if entry link matches `sifus/elfeed-visit-patterns',
visit otherwise."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode)
                   elfeed-show-entry
                 (elfeed-search-selected :single)))
        (patterns sifus/elfeed-visit-patterns))
    (while (and patterns (not (string-match (caar patterns) (elfeed-entry-link entry))))
      (setq patterns (cdr patterns)))
    (cond
     (patterns
      (elfeed-untag entry 'unread)
      (elfeed-search-update-entry entry)
      (funcall (cdar patterns)))
     ((eq major-mode 'elfeed-search-mode)
      (call-interactively 'elfeed-search-show-entry))
   (t (elfeed-show-visit)))))

(map! :map elfeed-search-mode-map
      :after elfeed-search
      :n "RET" #'sifus/elfeed-visit-maybe-external
      :n "S-RET" #'elfeed-search-show-entry)
(map! :map elfeed-show-mode-map
      :after elfeed-show
      :n "S-RET" #'elfeed-show-visit)

; Auth-source
(after! auth-source
  (require 'secrets)
  (when (and secrets-enabled (secrets-list-collections))
    (add-to-list 'auth-sources "secrets:Passwords")))

; Popups
;; (set-popup-rules!
;;   '(("^\\*info\\*"
;;      :slot 2 :side left :width 83 :quit nil)))

; Calibre
(use-package! calibredb
  :config
  (setq calibredb-root-dir "~/Documents/Calibre Library"
        calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  ; Snipe really gets in Calibre's way
  (after! evil-snipe
    (add-to-list 'evil-snipe-disabled-modes 'calibredb-search-mode))
  (map! :map doom-leader-open-map
        :desc "Calibre" "c" #'calibredb)
  ; TODO check if needed
  ; As always, mostly stolen from Tecosaur
  ;; (map! :map calibredb-show-mode-map
  ;;       :ne "?" #'calibredb-entry-dispatch
  ;;       :ne "o" #'calibredb-find-file
  ;;       :ne "O" #'calibredb-find-file-other-frame
  ;;       :ne "V" #'calibredb-open-file-with-default-tool
  ;;       :ne "s" #'calibredb-set-metadata-dispatch
  ;;       :ne "e" #'calibredb-export-dispatch
  ;;       :ne "q" #'calibredb-entry-quit
  ;;       :ne "." #'calibredb-open-dired
  ;;       :ne [tab] #'calibredb-toggle-view-at-point
  ;;       :ne "M-t" #'calibredb-set-metadata--tags
  ;;       :ne "M-a" #'calibredb-set-metadata--author_sort
  ;;       :ne "M-A" #'calibredb-set-metadata--authors
  ;;       :ne "M-T" #'calibredb-set-metadata--title
  ;;       :ne "M-c" #'calibredb-set-metadata--comments)
  ;; (map! :map calibredb-search-mode-map
  ;;       :ne [mouse-3] #'calibredb-search-mouse
  ;;       :ne "RET" #'calibredb-view
  ;;       :ne "?" #'calibredb-dispatch
  ;;       :ne "a" #'calibredb-add
  ;;       :ne "A" #'calibredb-add-dir
  ;;       :ne "c" #'calibredb-clone
  ;;       :ne "d" #'calibredb-remove
  ;;       :ne "D" #'calibredb-remove-marked-items
  ;;       :ne "j" #'calibredb-next-entry
  ;;       :ne "k" #'calibredb-previous-entry
  ;;       :ne "l" #'calibredb-virtual-library-list
  ;;       :ne "L" #'calibredb-library-list
  ;;       :ne "n" #'calibredb-virtual-library-next
  ;;       :ne "N" #'calibredb-library-next
  ;;       :ne "p" #'calibredb-virtual-library-previous
  ;;       :ne "P" #'calibredb-library-previous
  ;;       :ne "s" #'calibredb-set-metadata-dispatch
  ;;       :ne "S" #'calibredb-switch-library
  ;;       :ne "o" #'calibredb-find-file
  ;;       :ne "O" #'calibredb-find-file-other-frame
  ;;       :ne "v" #'calibredb-open-file-with-default-tool
  ;;       :ne "." #'calibredb-open-dired
  ;;       :ne "b" #'calibredb-catalog-bib-dispatch
  ;;       :ne "e" #'calibredb-export-dispatch
  ;;       :ne "y" #'calibredb-org-link-copy
  ;;       :ne "r" #'calibredb-search-refresh-and-clear-filter
  ;;       :ne "R" #'calibredb-search-clear-filter
  ;;       :ne "q" #'calibredb-search-quit
  ;;       :ne "m" #'calibredb-mark-and-forward
  ;;       :ne "f" #'calibredb-filter-dispatch
  ;;       :ne "F" #'calibredb-toggle-favorite-at-point
  ;;       :ne "x" #'calibredb-toggle-archive-at-point
  ;;       :ne "h" #'calibredb-toggle-highlight-at-point
  ;;       :ne "u" #'calibredb-unmark-and-forward
  ;;       :ne "i" #'calibredb-edit-annotation
  ;;       :ne "DEL" #'calibredb-unmark-and-backward
  ;;       :ne [backtab] #'calibredb-toggle-view
  ;;       :ne [tab] #'calibredb-toggle-view-at-point
  ;;       :ne "M-n" #'calibredb-show-next-entry
  ;;       :ne "M-p" #'calibredb-show-previous-entry
  ;;       :ne "/" #'calibredb-search-live-filter
  ;;       :ne "M-t" #'calibredb-set-metadata--tags
  ;;       :ne "M-a" #'calibredb-set-metadata--author_sort
  ;;       :ne "M-A" #'calibredb-set-metadata--authors
  ;;       :ne "M-T" #'calibredb-set-metadata--title
  ;;       :ne "M-c" #'calibredb-set-metadata--comments))
  )

; TV and Movies
(use-package! orgmdb
  :config
  ; TODO Fix this, probably upstream
  (setq orgmdb-omdb-apikey "" ;(auth-source-pick-first-password :Title "OMDB API Key")
        orgmdb-fill-property-list '(genre rated runtime director country imdb-id imdb-rating metascore actors plot)))
