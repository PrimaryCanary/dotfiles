;; -*- lexical-binding: t; -*-

(package! auto-activating-snippets
  :recipe (:host github :repo "ymarco/auto-activating-snippets")
  :pin "118ed7f")
(package! laas
  :recipe (:host github :repo "tecosaur/LaTeX-auto-activating-snippets")
  :pin "14c6cc2ff8c0c6b20b83fb075b94a8661a985249")
;(package! org-fragtog)
;(package! toc-org)
(package! calibredb :pin "cb93563d0ec9e0c653210bc574f9546d1e7db437")
(package! orgmdb
  :recipe (:host github :repo "isamert/orgmdb.el")
  :pin "c8ffec0bf4596249562ef4ab39d9cd3cab40ba35")
