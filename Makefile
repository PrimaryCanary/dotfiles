MODULES = distrobox emacs backup beets discord docker git keepassxc mpv pia \
          scripts ssh syncthing termux vim vscode zsh jetbrains thunderbird gnome \
		  init-repo

DOTS = ./dots
OS = fedora
SHOULD_INSTALL = ""
INSTALL = [ ! $(SHOULD_INSTALL) ] || # Invert so make doesn't fail with non-install
UNINSTALL = [ ! $(SHOULD_INSTALL) ] || # This comment adds extra whitespace to guarantee separator

DISTROBOXES = base desktop-environment dev dev-python dev-ansible
DISTROBOX_RUNNER = podman

AGE_DOTFILES_KEY = ~/.local/share/age/dotfiles.txt
AGE_DECRYPT = age -d -i $(AGE_DOTFILES_KEY)

DOTS_DOOM_DIR = $(DOTS)/emacs/.config/doom
ENCRYPTED_FILES = $(DOTS_DOOM_DIR)/feeds.org \
	$(DOTS_DOOM_DIR)/feeds.el \
	$(DOTS)/backup/.config/rclone/rclone.conf \
	$(DOTS)/ssh/.ssh/config

STOW = $(SYS_BIN_PATH)/stow
STOW_TARGET = ~
STOW_LINK_BASE = $(STOW) -d $(DOTS) -t $(STOW_TARGET) -S
STOW_LINK = $(STOW_LINK_BASE) $@

include Makefile.$(OS)

# TODO: Flatpak infrastructure
# TODO: more granular, less hardcoded Stow targets; maybe .dotfile_target in each directory

.PHONY: $(MODULES) $(PKGMAN_RULE) age stow $(DISTROBOXES) age-diff

all: $(MODULES)

package_var = $*
$(SYS_BIN_PATH)/%:
	$(INSTALL) $($(package_var)_PKGS)

# Make everything depend on Stow, the package manager, and Age
$(MODULES): $(SYS_BIN_PATH)/stow $(PKGMAN_RULE) age
$(PKGMAN_RULE): $(SYS_BIN_PATH)/stow

AGE_DIFF_CMD = ./git-age-diff
init-repo:
	git config diff.age.textconv '$(AGE_DIFF_CMD)'

dnf: $(STOW_TARGET)/.config/dnf $(DNF_REPOS)
$(STOW_TARGET)/.config/dnf:
	$(STOW_LINK_BASE) dnf
$(REPOS_DIR)/rpmfusion-%.repo:
	$(INSTALL) "https://mirrors.rpmfusion.org/$*/fedora/rpmfusion-$*-release-$$(rpm -E %fedora).noarch.rpm"
$(REPOS_DIR)/docker-ce.repo:
	sudo $(DNF) config-manager --add-repo 'https://download.docker.com/linux/fedora/docker-ce.repo'

distrobox: $(DISTROBOXES) $(SYS_BIN_PATH)/distrobox
$(DISTROBOXES): %: distroboxes/Dockerfile.%
	$(DISTROBOX_RUNNER) build -q -t "$*" -f "$<" distroboxes
	distrobox create -n "$@" -i "localhost/$*"

age: $(SYS_BIN_PATH)/age
$(AGE_DOTFILES_KEY):
	@echo "Ensure the dotfiles private key is accessible at $(AGE_DOTFILES_KEY)"
$(ENCRYPTED_FILES): %: %.age $(SYS_BIN_PATH)/age $(AGE_DOTFILES_KEY)
# TODO: edit encrypted files more easily
	if [ -f "$@" ]; then chmod 600 "$@"; fi
	$(AGE_DECRYPT) -o "$@" "$<"
	chmod 400 "$@"
	chmod 600 "$<"
age-diff:
	@$(AGE_DECRYPT) "$(file)"

git: $(SYS_BIN_PATH)/git
	$(STOW_LINK)

EMACS_BINS = $(SYS_BIN_PATH)/emacs
EMACS_CONFIG_DIR = $(STOW_TARGET)/.config/emacs
DOOM_BIN = $(EMACS_CONFIG_DIR)/bin/doom
emacs: $(DOTS_DOOM_DIR)/feeds.org $(DOTS_DOOM_DIR)/feeds.el $(EMACS_BINS) $(EMACS_CONFIG_DIR)/shell.nix
	rm -f ~/.emacs # Keep Emacs from creating it's own init
	$(STOW_LINK)
	$(DOOM_BIN) sync
$(EMACS_CONFIG_DIR)/shell.nix:
	$(STOW_LINK_BASE) emacs
	$(DOOM_BIN) install

backup: package_var = backup
backup: $(DOTS)/backup/.config/rclone/rclone.conf $(SYS_BIN_PATH)/restic
	$(STOW_LINK)
	chmod 600 $(DOTS)/backup/.config/rclone/rclone.conf # So rclone can get new auth tokens
	systemctl --user daemon-reload
	systemctl --user enable --now backup.timer

beets: $(SYS_BIN_PATH)/beets $(STOW_TARGET)/.config/beets/library.db
	$(STOW_LINK)
$(STOW_TARGET)/.config/beets/library.db:
	beet import ~/Music

discord: $(SYS_BIN_PATH)/discord
	$(STOW_LINK)

docker: $(SYS_BIN_PATH)/docker
	$(STOW_LINK)
	sudo usermod -aG docker "$$(whoami)"

keepassxc: $(SYS_BIN_PATH)/keepassxc
	$(STOW_LINK)
	[ ! -f $(SYS_BIN_PATH)/gnome-keyring-daemon ] || $(GNOME_REMOVE_KEYRING)
	systemctl --user daemon-reload
	systemctl --user enable --now keepassxc.service

MPV_CONFIG_DIR = $(STOW_TARGET)/.config/mpv
MPV_SCRIPT_DIR = $(MPV_CONFIG_DIR)/scripts
MPV_PLUGIN_SYMLINKS = $(MPV_SCRIPT_DIR)/quality-menu.lua
mpv: $(SYS_BIN_PATH)/mpv $(MPV_PLUGIN_SYMLINKS)
	$(STOW_LINK)
	systemctl --user daemon-reload
	systemctl --user enable --now mpv
$(MPV_SCRIPT_DIR)/%.lua:
	mkdir -p $(MPV_SCRIPT_DIR)
	cd $(MPV_SCRIPT_DIR) && ln -sf "$*/$*.lua" .

pia: /opt/piavpn/bin/pia-client
/opt/piavpn/bin/pia-client: pia-linux.run
	$(SHELL) "$<"
pia-linux.run:
	curl -sSL -o "$@" 'https://installers.privateinternetaccess.com/download/pia-linux-3.3.1-06924.run'
	chmod u+x "$@"

scripts:
	$(STOW_LINK)

ssh: $(SYS_BIN_PATH)/ssh $(DOTS)/ssh/.ssh/config
	$(STOW_LINK)

syncthing: $(SYS_BIN_PATH)/syncthing
	mkdir -p $(SYNCTHING_FOLDERS)
# TODO: Use REST API to set up folders
# for f in $(SYNCTHING_FOLDERS); do curl ; done
	systemctl --user enable --now syncthing

# TODO
termux:

zsh: $(SYS_BIN_PATH)/zsh
	$(STOW_LINK)
	grep -q "^$$(whoami):.*:$$(which zsh)$$" /etc/passwd || \
		sudo usermod --shell "$$(which zsh)" "$$(whoami)"

# TODO
jetbrains:

# TODO
thunderbird:

# TODO: Flameshot install (flatpak or package?), mpv default video player, Dracula theme
# TODO: gnome-terminal starts in Distrobox
GSET_SET = gsettings set
GNOME_KEYBINDS = org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom
GNOME_SETTINGS =  'org.gnome.desktop.wm.preferences audible-bell false' # Kill the fucking bloop
GNOME_SETTINGS += 'org.gnome.desktop.peripherals.touchpad tap-to-click true'
GNOME_SETTINGS += 'org.gnome.settings-daemon.plugins.power ambient-enabled false' # No adaptive brightness
GNOME_SETTINGS += 'org.gnome.desktop.interface show-battery-percentage true'
GNOME_SETTINGS += 'org.gnome.desktop.interface clock-show-seconds true'
GNOME_SETTINGS += 'org.gnome.desktop.interface clock-format "12h"' # AM/PM clock
GNOME_SETTINGS += "org.gnome.settings-daemon.plugins.media-keys screenshot '[]'" # Disable PrtSc binding
GNOME_SETTINGS += "$(GNOME_KEYBINDS)0/ name 'Flameshot Screenshot'" # Flameshot binding
GNOME_SETTINGS += "$(GNOME_KEYBINDS)0/ command 'flatpak run org.flameshot.Flameshot gui'"
GNOME_SETTINGS += "$(GNOME_KEYBINDS)0/ binding 'Print'"

GNOME_EXTENSION_DIR = ~/.local/share/gnome-shell/extensions
GNOME_EXTENSIONS =  appindicatorsupport@rgcjonas.gmail.com
GNOME_EXTENSIONS += gsconnect@andyholmes.github.io # Look into KDE Connect on GNOME
GNOME_EXTENSIONS += user-theme@gnome-shell-extensions.gcampax.github.com

gnome: package_var = gnome
gnome: $(SYS_BIN_PATH)/gnome-shell $(GNOME_EXTENSIONS)
	$(UNINSTALL) $(GNOME_UNINSTALL_PKGS)
	for setting in $(GNOME_SETTINGS); do echo "$$setting" | xargs $(GSET_SET); done
$(GNOME_EXTENSIONS): %: $(GNOME_EXTENSION_DIR)/%/metadata.json
$(GNOME_EXTENSION_DIR)/%/metadata.json:
	gdbus call --session \
               --dest org.gnome.Shell.Extensions \
               --object-path /org/gnome/Shell/Extensions \
               --method org.gnome.Shell.Extensions.InstallRemoteExtension \
               "$*"
	gnome-extensions enable "$*"
