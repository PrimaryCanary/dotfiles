Vagrant.configure('2') do |config|
  VMS = ["vesuvius", "mauna-loa"]
  USER = "vagrant"

  if Vagrant.has_plugin?('vagrant-cachier')
    config.cache.scope = 'machine'
  end

  (0..VMS.length).each do |i|
    host = VMS[i]
    config.vm.define "#{host}" do |c|
      c.vm.box = "generic/fedora35"

      c.ssh.insert_key = true
      c.ssh.keep_alive = true
      c.ssh.remote_user = USER
      c.vm.hostname = host

      c.vm.network "private_network", :ip => "10.10.10.#{10 + i}"

      # The folder is read-only on the host. See links for info.
      # https://eklitzke.org/fedora-qemu-and-9p-filesystem-passthrough
      # https://listman.redhat.com/archives/libvirt-users/2014-April/006370.html
      # https://bugzilla.redhat.com/show_bug.cgi?id=1346100
      SYNCED_FOLDER_DEST = "/#{USER}"
      c.vm.synced_folder ".", SYNCED_FOLDER_DEST, type: "9p", accessmode: "squash"

      # Use the provisioner for installation only
      c.vm.provision "ansible_local", run: "once" do |ans|
        ans.playbook_command = "echo ansible-playbook"
        ans.playbook = "dotfiles.yml"
      end

      config.vm.provision 'key', type: 'file', source: "~/.local/share/age/dotfiles.txt", destination: "$HOME/.local/share/age/dotfiles.txt"

      # Solve above read-only issue by copying and bind mounting
      DOTS_DIR = "/home/#{USER}/dots"
      c.vm.provision "file", source: "dots/", destination: DOTS_DIR, run: "always"
      c.vm.provision "shell",
                     inline: "sudo mount --bind #{DOTS_DIR} #{SYNCED_FOLDER_DEST}/dots",
                     run: "always"

      c.vm.provider "libvirt" do |libvirt, override|
        libvirt.memory = 1024
        libvirt.cpus = 1
        libvirt.uri = "qemu:///system"
      end
    end
  end
end
